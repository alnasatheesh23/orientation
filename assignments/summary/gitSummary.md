## What is Git? How it works?

Git is an **Open Source Distributed Version Control System**. Also, Git provides features like branches and merges, which I will be covering later. Distributed Version Control System: Git has a remote repository which is stored in a server and a local repository which is stored in the computer of each developer.To start working with Git, you just need to run the git init command. It turns the current directory into the Git working directory and creates the repository in the . git (hidden) directory it creates there. With this command, a change in the working directory is added to the staging area for the next commit.

## To Start a new git repository

-Create a directory to contain the project.
1. Go into the new directory.
1. Type git init .
1. Write some code.
1. Type git add to add the files (see the typical use page).
1. Type git commit .

## Basic workflow of Git.

1. You modify a file from the working directory. 
1. You add these files to the staging area. 
1. You perform commit operation that moves the files from the staging area.

![Basic Git Workflow](https://gitlab.com/iotiotdotin/project-internship/orientation/-/wikis/extras/Git.png)



Git's data is like a series of snapshots of a miniature filesystem. With Git, every time you commit, or save the state of your project, Git basically takes a picture of what all your files look like at that moment and stores a reference to that snapshot,Git doesn’t store the file again only the link to the previous identical file it has already stored.

## Git Terms

### Branch
A version of the repository that diverges from the main working project. Branches can be a new version of a repository, experimental changes, or personal forks of a repository for users to alter and test changes.
<br>
### Checkout
The git checkout command is used to switch branches in a repository.
<br>
### Clone
A clone is a copy of a repository or the action of copying a repository. When cloning a repository into another branch, the new branch becomes a remote-tracking branch that can talk upstream to its origin branch (via pushes, pulls, and fetches).
<br>
### Fetch
By performing a Git fetch, we are downloading and copying that branch’s files to our workstation. Multiple branches can be fetched at once, and we can rename the branches when running the command to suit our needs.
<br>
### HEAD
HEAD is a reference variable used to denote the most current commit of the repository in which we are working. When we add a new commit, HEAD will then become that new commit.
<br>
### Master
The primary branch of all repositories. All committed and accepted changes should be on the master branch. We can work directly from the master branch, or create other branches.
<br>
### Merge
Taking the changes from one branch and adding them into another (traditionally master) branch. These commits are usually first requested via pull request before being merged by a project maintainer.
<br>
### Origin
The conventional name for the primary version of a repository. Git also uses *origin* as a system alias for pushing and fetching data to and from the primary branch. For example, *git push origin master*, when run on a remote, will push the changes to the master branch of the primary repository database.
<br>
### Pull/Pull Request
If someone has changed code on a separate branch of a project and wants it to be reviewed to add to the master branch, that someone can put in a pull request. Pull requests ask the repo maintainers to review the commits made, and then, if acceptable, merge the changes upstream. A pull happens when adding the changes to the master branch.
<br>
### Push
Updates a remote branch with the commits made to the current branch. We are literally “pushing” our changes onto the remote.
<br>
### Rebase
When rebasing a git commit, we can split the commit, move it, squash it if unwanted, or effectively combine two branches that have diverged from one another.
<br>
### Remote
A copy of the original branch. When we clone a branch, that new branch is a remote, or clone. Remotes can talk to the origin branch, as well as other remotes for the repository, to make communication between working branches easier.


## The Three Stages

Git has three main states-

1. **Modified** means that you have changed the file but have not committed it to your database yet.
1. **Staged** means that you have marked a modified file in its current version to go into your next commit snapshot.
1. **Committed** means that the data is safely stored in your local database.

This leads us to the three main sections of a Git project:
the working tree, the staging area, and the Git directory.



### Commands in Git

* git config
  * Usage: git config –global user.name “[name]”  
      -This command sets the author name and email address respectively to be used with your commits.

* git init
  * Usage: git init [repository name]
      -This command is used to start a new repository.

* git clone
  * Usage: git clone [url]  
      -This command is used to obtain a repository from an existing URL.

* git add
  * Usage: git add [file]  
       -This command adds a file to the staging area.

* git commit
  * Usage: git commit -m “[ Type in the commit message]”  
       -This command records or snapshots the file permanently in the version history.

* git diff
  * Usage: git diff  
       -This command shows the file differences which are not yet staged.

* git reset
  * Usage: git reset [file]  
        -This command unstages the file, but it preserves the file contents.

* git status
  * Usage: git status  
         -This command lists all the files that have to be committed.

* git rm
  * Usage: git rm [file]  
        -This command deletes the file from your working directory and stages the deletion.
<h3>GitLab</h3>

- Gitlab is a service that provides remote access to **Git repositories**. In addition to hosting your code, the services provide additional features designed to help manage the software development lifecycle.
- Open source self-hosted Git management software. 
- GitLab offers **git repository management, code reviews, issue tracking, activity feeds and wikis.**

### GitLab Interface

![GitLab](extras/01.png)


### GitLab basics

The following are guides to basic GitLab functionality:
1. Create and add your SSH public key, for enabling Git over SSH.
1. Create a project, to start using GitLab.
1. Create a group, to combine and administer projects together.
1. Create a branch, to make changes to files stored in a project’s repository.
1. Feature branch workflow.
1. Fork a project, to duplicate projects so they can be worked on in parallel.
1. Add a file, to add new files to a project’s repository.
1. Create an issue, to start collaborating within a project.
1. Create a merge request, to request changes made in a branch be merged into a project’s repository.


