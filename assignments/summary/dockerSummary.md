# Docker summary
### Docker overview

1. Docker is an open platform for developing, shipping, and running applications. 
1. Docker enables you to separate your applications from your infrastructure so you can deliver software quickly. With Docker, you can manage your infrastructure in the same ways you manage your applications. 
1. By taking advantage of Docker’s methodologies for shipping, testing, and deploying code quickly, you can significantly reduce the delay between writing code and running it in production.


### The Docker platform

Docker provides tooling and a platform to manage the lifecycle of your containers:
1. Develop your application and its supporting components using containers.
2. The container becomes the unit for distributing and testing your application.
3. When you’re ready, deploy your application into your production environment, as a container or an orchestrated service. This works the same whether your production environment is a local data center, a cloud provider, or a hybrid of the two.

### Docker Engine
Docker Engine is a client-server application with these major components: <br> 
1. A server which is a type of long-running program called a daemon process (the dockerd command). <br>
2. A REST API which specifies interfaces that programs can use to talk to the daemon and instruct it what to do. <br>
3. A command line interface (CLI) client (the docker command). <br>

![Engine Components Flow](https://docs.docker.com/engine/images/engine-components-flow.png)

The CLI uses the Docker REST API to control or interact with the Docker daemon through scripting or direct CLI commands. Many other Docker applications use the underlying API and CLI. The daemon creates and manages Docker objects, such as images, containers, networks, and volumes.


**Docker’s container-based platform** allows for highly portable workloads. Docker containers can run on a developer’s local laptop, on physical or virtual machines in a data center, on cloud providers, or in a mixture of environments.

Docker’s portability and lightweight nature also make it easy to dynamically manage workloads, scaling up or tearing down applications and services as business needs dictate, in near real time.

<h3>Running more workloads on the same hardware</h3>

Docker is lightweight and fast. It provides a viable, cost-effective alternative to hypervisor-based virtual machines, so you can use more of your compute capacity to achieve your business goals. Docker is perfect for high density environments and for small and medium deployments where you need to do more with fewer resources.

<h3>Docker architecture</h3>
Docker uses a client-server architecture. The Docker client talks to the Docker daemon, which does the heavy lifting of building, running, and distributing your Docker containers. The Docker client and daemon can run on the same system, or you can connect a Docker client to a remote Docker daemon. The Docker client and daemon communicate using a REST API, over UNIX sockets or a network interface.

<h3>The Docker daemon</h3>

1. The Docker daemon (dockerd) listens for Docker API requests and manages Docker objects such as images, containers, networks, and volumes. 
1. A daemon can also communicate with other daemons to manage Docker services.


<h3>The Docker client</h3>

1. The Docker client (docker) is the primary way that many Docker users interact with Docker. 
1. When you use commands such as docker run, the client sends these commands to dockerd, which carries them out. 
1. The docker command uses the Docker API. 
1. The Docker client can communicate with more than one daemon.

<h3>Docker registries</h3>

1. A Docker registry stores Docker images. 
1. Docker Hub is a public registry that anyone can use.
1. Docker is configured to look for images on Docker Hub by default. You can even run your own private registry.


<h3>Docker objects</h3>
When you use Docker, you are creating and using images, containers, networks, volumes, plugins, and other objects. This section is a brief overview of some of those objects.

<h3>Images</h3>
An image is a read-only template with instructions for creating a Docker container. Often, an image is based on another image, with some additional customization. For example, you may build an image which is based on the ubuntu image, but installs the Apache web server and your application, as well as the configuration details needed to make your application run.

<h3>Containers</h3>
A container is a runnable instance of an image. You can create, start, stop, move, or delete a container using the Docker API or CLI. You can connect a container to one or more networks, attach storage to it, or even create a new image based on its current state.

 
 
 
 **Docker Hub:** It is basically GitHub but for Docker Images.

 **Installing the Docker Engine:** [Official documentation: Installing Docker Engine](https://docs.docker.com/engine/install/ubuntu/)



 ### Docker commands

 #### View all containers running on Docker's host

`docker ps`

#### Start any stopped containers

`docker start <container-name or container-id>`

#### Stop any running containers

`docker stop <container-name or beginning-of-container-id>` 

#### Create containers from Docker Images

`docker run <container-name>`

#### Delete a container

`docker rm <container-name>`

#### Download/pull Docker Images

`docker pull <image-author>/<image-name>`

#### Run Docker Image as a bash script

`docker run -ti <image-author>/<image-name> /bin/bash`

#### Copy any file inside Docker container with <container-id>

`docker cp <code-filename> <container-id>:/`

Furthermore, write a bash script say `install-dependencies.sh` to install all dependencies for successful execution of <code-filename> 

For instance, say <code-filename> was a basic python `.py` executable

`install-dependencies.sh` will include:

	apt update
	apt install python3

Now, copy the bash script into the same Docker container too

`docker cp install-dependencies.sh <container-id>:/`

#### Installing dependencies

 - Allow running bash script as executable 
 
 `docker exec -it <container-id> chmod +x install-dependencies.sh`
	
 - Install dependencies
 
 `docker exec -it <container-id> /bin/bash ./install-dependencies.sh`
	
#### Run program inside the container

Start the container

`docker start <container-id>`

For the example given above, the following will be the line for execution

`docker exec <container-id> python3 <code-filename>`

#### Save copied program inside Docker Image

`docker commit <container-id> <image-author>/<image-name>`

#### Tag Docker Image with a different name

`docker tag <image-author>/<image-name> <user-name>/<repo-name>`

#### Push Docker Image into DockerHub

`docker push <user-name>/<repo-name>`

